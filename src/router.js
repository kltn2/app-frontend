import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "dashboard",
      exact: true,
      component: () => import("./components/Dashboard.vue"),
    },
    {
      path: "/sale",
      name: "sale",
      exact: true,
      component: () => import("./pages/Sale.vue"),
    },
    {
      path: "/livestream",
      name: "livestream",
      exact: true,
      component: () => import("./pages/Livestream.vue"),
    },
    {
      path: "/order",
      name: "orderList",
      exact: true,
      component: () => import("./pages/OrderList.vue"),
    },
    {
      path: "/marketing",
      name: "marketing",
      exact: true,
      component: () => import("./pages/Marketing.vue"),
    },
    {
      path: "/messenger",
      name: "messenger",
      exact: true,
      component: () => import("./pages/Messenger.vue"),
    },
    {
      path: "/products",
      name: "products",
      exact: true,
      component: () => import("./pages/Product.vue"),
    },
    {
      path: "/customers",
      name: "customers",
      exact: true,
      component: () => import("./pages/Customer.vue"),
    },
    {
      path: "/posts",
      name: "posts",
      exact: true,
      component: () => import("./pages/Post.vue"),
    },
    {
      path: "/blocklist",
      name: "blockList",
      exact: true,
      component: () => import("./pages/BlockList.vue"),
    },
    {
      path: "/chatbot",
      name: "Chatbot",
      exact: true,
      component: () => import("./pages/ChatBot.vue"),
    },
    {
      path: "/setting",
      name: "setting",
      exact: true,
      component: () => import("./pages/Setting.vue"),
    },
    {
      path: "/report",
      name: "report",
      exact: true,
      component: () => import("./pages/Report.vue"),
    },
    {
      path: "/auditinglog",
      name: "auditingLog",
      exact: true,
      component: () => import("./pages/AuditingLog.vue"),
    },
  ],
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});

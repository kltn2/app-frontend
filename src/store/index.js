import Vue from "vue";
import Vuex from "vuex";
import user from "./user";
import sale from "./sale";
import product from "./product";
import order from "./order";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

export default new Vuex.Store({
  modules: {
    user,
    sale,
    product,
    order,
  },
  strict: debug,
});

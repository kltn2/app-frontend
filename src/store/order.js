import axios from "axios";

const state = () => ({
  orders: [],
});

const getters = {};

const mutations = {
  setOrders(state, payload) {
    state.orders = payload;
  },
};

const actions = {
  async getOrders({ commit }) {
    const res = await axios.get("assets/demo/data/order/orders.json");
    const { orders } = res.data;
    if (orders.length) {
      await commit("setOrders", orders);
    }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};

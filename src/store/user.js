const state = () => ({
  user: null,
});

const getters = {
  fullName: (state) => `${state.user.firstName} ${state.user.lastName}`,
};

const mutations = {
  setUser(state, payload) {
    state.user = payload;
  },
};

const actions = {
  login({ commit }, payload) {
    commit("setUser", payload);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};

import axios from "axios";

const state = () => ({
  products: [],
  categories: [],
  units: [],
});

const getters = {};

const mutations = {
  setProducts(state, payload) {
    state.products = payload;
  },

  setCategories(state, payload) {
    state.categories = payload;
  },

  setUnits(state, payload) {
    state.units = payload;
  },
};

const actions = {
  async getProducts({ commit }) {
    const res = await axios.get("assets/demo/data/sale/products.json");
    const { products } = res.data;

    if (products.length) {
      commit("setProducts", products);
    }
  },

  async getAllProducts({ commit }) {
    const res = await axios.get("assets/demo/data/product/products.json");
    const { products } = res.data;

    if (products.length) {
      commit("setProducts", products);
    }
  },

  async getCategories({ commit }) {
    const res = await axios.get("assets/demo/data/product/categories.json");
    const { categories } = res.data;

    if (categories.length) {
      commit("setCategories", categories);
    }
  },

  async getUnits({ commit }) {
    const res = await axios.get("assets/demo/data/product/units.json");
    const { units } = res.data;

    if (units.length) {
      commit("setUnits", units);
    }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};

export const filtersButton = [
  { name: "Tất cả", value: "all" },
  { name: "Chờ giao hàng", value: "waiting" },
  { name: "Đang giao hàng", value: "going" },
  { name: "Chờ thu COD", value: "cod" },
  { name: "Hoàn thành", value: "finish" },
  { name: "Thất bại", value: "miss" },
  { name: "Đã chuyển khoản", value: "transfer" },
  { name: "Đã huỷ", value: "cancel" },
];
export default { filtersButton };
